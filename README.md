# Dijkstra

The Dijkstra algorithm allows to calculate the shortest path between two vertices.

https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

## Dependencies

* Python 3 (https://www.python.org/downloads/)

## Terminal

Dijkstra.py Graph.json Departure Arrived 

* The first argument is the name of the program, here Dijkstra.py</li>
* The second argument contains our graph, it is represented in a json file</li>
* The third argument is departure, where do you want to go ?</li>
* The fourth argument is arrival, where do you want to arrive ?</li>

### **Windows**
<pre>
    Dijkstra.py graph.json B C
</pre>

### **Linux**
<pre> 
    chmod +x Dijkstra.py
    ./Dijkstra.py graph.json B C
</pre>

### **Mac**
<pre>
    python3 Dijkstra.py graph.json B C
</pre>
    
## Script

Consult the "script.py" file to understand how the Disjktra module works

### **Windows**
<pre>
    script.py
</pre>

### **Linux**
<pre> 
    chmod +x script.py
    ./script.py
</pre>

### **Mac**
<pre>
    python3 script.py
</pre>

## Example

![alt text](http://cs.lmu.edu/~ray/images/agraph.png)

### Understand, read, analyze a graph

*  The letters represented in blue are **vertices**
*  Numbers are **values**
*  The lines that connect the vertices are **edges**

### Representation of the graph in json
```json
// graph.json

{

    "A": [
        ["B", 3], 
        ["J", 4], 
        ["G", 1]
    ], 
            
        
    "B": [
        ["A", 3],
        ["D", 10]
    ],
            
        
    "D": [
        ["B", 10],
        ["J", 3],
        ["H", 11]
    ],
            
        
    "J": [
        ["A", 4],
        ["D", 3],
        ["G", 6]
    ],
            
        
    "G": [
        ["A", 1],
        ["J", 6],
        ["F", 8],
        ["E", 14]
    ],
            
        
    "E": [
        ["G", 14],
        ["F", 2],
        ["I", 1]
    ],
            
        
    "I": [
        ["E", 1],
        ["F", 2],
        ["H", 6]
    ],
            
        
    "F": [
        ["G", 8],
        ["E", 2],
        ["I", 2],
        ["H", 4]
    ],
            
        
    "H": [
        ["D", 11],
        ["F", 4],
        ["I", 6],
        ["C", 3]
    ],
            
        
    "C": [
        ["H", 3]
    ]
}
```

## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/