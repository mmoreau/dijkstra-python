#!/usr/bin/python3

import os
import sys
import random
import itertools
import json
from operator import itemgetter
from Dijkstra import Dijkstra


class App:


	@classmethod
	def way(cls):

		""" Randomly select a starting point and a destination point and calculate the shortest route corresponding to the starting and destination points. """

		graphKeys = G.keys()

		departure = random.choice(list(graphKeys))
		arrived = random.choice(list(graphKeys - set(departure)))

		sys.stdout.write(f"Departure : {departure}\n")
		sys.stdout.write(f"Arrived   : {arrived}\n\n")
		sys.stdout.write(f"{Dijkstra.Dijkstra(G, departure, arrived)}\n\n")



	@classmethod
	def way2(cls):

		""" Calculates the shortest path of all nodes (combination). """

		[sys.stdout.write(f"{element} -> {Dijkstra.Dijkstra(G, element[0], element[1])}\n") for element in list(itertools.combinations(list(G.keys()), 2))]



	@classmethod
	def way3(cls):

		""" Looks like the "way" function, with the difference that you manually choose a starting point and a destination point. """

		sys.stdout.write(f"{Dijkstra.Dijkstra(G, 'B', 'C')}\n")



	@classmethod
	def way4(cls):

		""" Sorting in ascending order the most priced paths in the graph. """

		graph = []

		for element in list(itertools.combinations(list(G.keys()), 2)):

			departure, arrived = element
			result = Dijkstra.Dijkstra(G, departure, arrived)
			weight = 0

			for element2 in enumerate(result[:-1]):
				for node in G[element2[1]]:
					if node[0] == result[(element2[0] + 1)]:
						weight += node[1]
						break

			graph.append((departure, arrived, weight))

		[sys.stdout.write(f"{result}\n") for result in sorted(graph, key=itemgetter(2))]


with open("graph.json", "r") as f:
	G = json.load(f)

# .:: Uncomment comments to call the function that interests you in relation to your needs ::.

App.way()
#App.way2()
#App.way3()
#App.way4()

if sys.platform.startswith('win32'):
	os.system("pause")