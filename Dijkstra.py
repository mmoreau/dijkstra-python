#!/usr/bin/python3

import os
import json
import sys

class Dijkstra:

	@classmethod
	def Dijkstra(cls, graph: dict, departure: str, arrived: str) -> list:

		"""Dijkstra : Finding the shortest route between two summits."""

		graphKeys = graph.keys()
		
		# Checking the arguments of the function
		lock = 0
		lock += 1 if graph else 0
		lock += 1 if departure in graphKeys else 0
		lock += 1 if arrived in graphKeys else 0

		#  If the arguments of the function are correct we will analyze the shortest path.
		if lock == 3:
			
			visited = {}
			graphMin = {departure:(departure, 0)}
			nodeCurrent = departure

			while nodeCurrent != arrived:

				# Run the elements of the node run
				for nodeElement in graph[nodeCurrent]:

					nameNodeElement, valNodeElement = nodeElement
					sumValNode = valNodeElement + graphMin[nodeCurrent][1]

					if nameNodeElement in visited.keys():
						if sumValNode < visited[nameNodeElement][1]:
							visited.update({nameNodeElement:(nodeCurrent, sumValNode)})
					else:
						visited.update({nameNodeElement:(nodeCurrent, sumValNode)})

				# Scroll the node that at the minimum value
				nodeMin = []

				for nodeElementMin in visited.keys() - graphMin.keys():
					if nodeMin:
						if nodeElementMin not in nodeMin[0]:
							if visited[nodeElementMin][1] < nodeMin[2]:
								nodeMin = [nodeElementMin, visited[nodeElementMin][0], visited[nodeElementMin][1]]
					else:
						nodeMin = [nodeElementMin, visited[nodeElementMin][0], visited[nodeElementMin][1]]

				# Replaces the following node
				nodeCurrent = nodeMin[0]

				# Adds the torque of the node with the lowest value
				graphMin.update({nodeMin[0]:(nodeMin[1], nodeMin[2])})

			# Displays the shortest path
			nodePath = [arrived]

			while nodePath[-1] != departure:
				nodePath.append(graphMin[nodePath[-1]][0])

			return nodePath[::-1]
		else:
			return []

try:
	args = sys.argv

	if len(args) == 4:
		if os.path.isfile(args[1]):
			with open(args[1], "r") as f:
				graph = json.load(f)
				sys.stdout.write(f"{Dijkstra.Dijkstra(graph, args[2], args[3])}\n")
except:
	pass